import re
import os
import pprint
import pandas as pd
from io import StringIO
from re import findall, search, MULTILINE

from glob import glob
from time import sleep
from datetime import datetime
from numpy import sqrt, median
from pandas import read_csv, DataFrame
# from wrapt_timeout_decorator import timeout

# @timeout(5)
# def execute_command(command):
#     """Wrapper for Python command line execution. A timeout is set in order to stop execution when an error is thrown by the command

#     Parameters
#     ----------
#     command : str
#         Command to be executed

#     Returns
#     -------
#     int
#         Integer (0) indicating if the process run correctly.
#     """
#     import os
#     return os.system(command)


def spm_parser(file_path, rpmview_path="C:\\Nanometrics\\Rpmview\\rpmview.exe"):
    """Extracts .dat file from .spm programmatically opening RPM Data Viewer

    Parameters
    ----------
    file_path : str
        Path to the SPM file
    rpmview_path : str
        Path to the rpmview.exe file

    Returns
    -------
    int
        Integer (0) indicating if the process run correctly.
    """
    try:
        os.system(f"{rpmview_path} {file_path} –PlFFT -Ascii_Col -Close")
        dat_path = glob(file_path.replace(".spm", "*.dat")).pop()
        with open(dat_path, "r") as f:
            text = f.read()
        limit_pos = search("X\s+,\s+Y,", text).start()
        data = "\n".join([x for x in text[:limit_pos].split("\n")][1:])
        data = findall("(^[^:]+):(.*?)\n", data, MULTILINE)
        data = {key.strip(): value.strip() for key, value in data}
        metrics = text[limit_pos:]
        metrics = read_csv(StringIO(metrics))
        metrics = {col.strip(): {"unit": metrics.loc[0, col].strip(), "data": list(map(float, metrics[col].drop(0)))} for col in metrics.columns if "Unnamed" not in col}
        data["data"] = metrics
        return data
    except Exception as e:
        print(e)
        return {}

def spm_cleaner(data, exclusion_zone=5):
    """Variables cleaner for .dat (generated from .spm) files
    Parameters
    ----------
    data : dict
        Python dictionary with variable and value pairs
    Returns
    -------
    dict
        Python dictionary with variable and value pairs cleaned
    """
    try:
        data = {key.lower().replace(" ", "_"): value for key, value in data.items()}
        try:
            data["run"], data["wafer"] = re.split("-|_| ", data["wafer_id"])
        except:
            data["run"] = data["wafer"] = None
        data["data"] = {key.lower().replace(" ", "_"): value for key, value in data["data"].items()}
        for key in ["thickness", "wafer_diam", "scan_diam", "resolution", "scan_rate", "laser", "power", "temperature", "exc_zone", "center_wavelength", "slit_width", "grating", ]:
            if key not in data:
                continue
            dc = {}
            dc["value"], dc["unit"] = data[key].split(" ")
            dc["value"] = float(dc["value"])
            data[key] = dc["value"]
        if data["exc_zone"] != 5:
            aux = DataFrame({key: value["data"] for key, value in data["data"].items()})
            limit = aux[["x", "y"]].abs().max().max() - exclusion_zone + 1
            aux["distance"] = sqrt((aux["x"] - 0)**2 + (aux["y"] - 0)**2)
            aux = aux[aux["distance"]<=limit].reset_index(drop = True)
            data["data"] = {key: {"unit": data["data"][key]["unit"], "data": aux[key].tolist()} for key, value in data["data"].items()}
            data["exc_zone"] = exclusion_zone
        data["pl_median"] = float(median(data["data"]["peak_lambda"]["data"]))
    except Exception as e:
        print(str(e))
    finally:
        return data






