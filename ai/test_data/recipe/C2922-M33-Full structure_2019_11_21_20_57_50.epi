#Bubblers: TMIn_1 and 2,  TMGa_1 and 2,

# PL 1388+-5 in CRIUS
# adjust T during step 282  to 625 +-1
# XRD wide: +- 250 arcsec
#SS<10cm-2

read Macro_Library;

####################################################################
##	GENERAL INFORMATION
##	System 701884 - CRIUS-As/P (IC platform)
##	experimentally 1cc-----2nm
##	updated from C2195
##	No StateMachine
##
##      BUBBLER CONDITIONS
##      TMGa_2 bubbler 1200 mbar
##      TMIn bubblers 400 mbar
##      TMGa: 5 °C ; TMAl: 20 °C ; TMIn: 25 °C ;
##
####################################################################


####################################################################
##      VARIABLES DEFINITION
####################################################################
##
##
##      TEMPERATURE CALIBRATION
##      Parameters used to calculate setpoints
##      for the requested surface temperatures

        variable Tcal_Factor = 1.136;
        variable Tcal_Offset = 89;
        variable ZoneA = 55;
        variable ZoneB = 60;
        variable ZoneC = 70;

        variable RampRate = 1.2;
        variable CoolRate = 0.9;

##      STATE-MACHINE CROSS-OVER POINTS
##      To optimise timing it is recommended
##      to match the StateMachine parameters

        variable Process_Start_Temp = 350;      # hydride start flowing
        variable Transfer_Auto_Temp = 300;      # hydride stop flowing at (Transfer_Auto_Temp + 50)
        variable Process_Start_ZoneA = 30;
        variable Process_Start_ZoneB = 50;
        variable Process_Start_ZoneC = 30;

        variable Process_Start_Press = 100;
        variable Transfer_Auto_Press = 50;
        variable Transfer_Auto_Flow = 12000;


##      GENERAL GROWTH CONDITIONS
        variable Growth_Temp = 776;
        variable Growth_Press = 132;
        variable Growth_Total_Flow = 48000;
	variable PH3_Stabilization = 800;

##	Oxide desorb
	variable Desorb_Temp = 810;
	variable Desorb_ZoneA = 51;
        variable Desorb_ZoneB = 58.5;
        variable Desorb_ZoneC = 65;
	variable Desorb_Press = 132;
	variable Desorb_PH3_flow = 800;
	variable Desorb_Time = 120;




	variable calib_factor_1=0.950;
	variable calib_factor_2=0.963;



##	InP buffer
	variable Buffer_Temp = 732.2; ##625
	variable Buffer_ZoneA = 53;
                variable Buffer_ZoneB = 63.5;
                variable Buffer_ZoneC = 72.6;
	variable Buffer_Press = 132;
	variable Buffer_PH3_flow = 800;
	variable Buffer_TMIn_1_flow = 696*calib_factor_1;
	variable Buffer_TMIn_2_flow = 696*calib_factor_2;
	variable Buffer_TMIn_Total_flow = 1500;
	variable TMIn_1_conc=2899*calib_factor_1;
	variable TMIn_2_conc=2873*calib_factor_2;


	variable Si_inject_1=75; #3E18, changed from 7e18 (M34) to 3 e18 (M33)
	variable Si_source_1 = 190;
	variable Si_dilute_1= 200;

	variable Si_inject_2=61; #1E18, ~12
	variable Si_source_2 = 50;
	variable Si_dilute_2= 200;


	variable Si_inject_3=104; #5E17, ~6.5
	variable Si_source_3 = 50;
	variable Si_dilute_3= 800;

	variable Si_inject_4=52; #2E17, ~3


	variable InP_Si_Time_1 = 1000*1.02/1;
	variable InP_Si_Time_2 = 1000*1.02/1;
	variable InP_Si_Time_3 = 1300*1.02/1;
	variable InP_Si_Time_4 = 200*1.02/1;
	variable InP_Spacer_bottom_Time = 25/0.5;
	variable InP_Spacer_1_Time = 125/0.5;
	variable InP_Spacer_2_Time = 50/0.5;
	variable InP_clad_Time = 105/0.5;

##	Barrier Q (1100nm PL, 0.15% Tensile) - TMGa_2,AsH3_2

	variable BarrierQ_AsH3_2_source_flow = 100;
	variable BarrierQ_AsH3_2_dilute_flow = 100;
	variable BarrierQ_AsH3_2_inject_flow = 28.94;
	variable AsH3_2_Total_flow = 200;
	variable BarrierQ_PH3_flow = 800;
	variable BarrierQ_TMIn_flow = 696*calib_factor_1;
	variable BarrierQ_TMGa_source_flow = 100;
	variable BarrierQ_TMGa_dilute_flow = 300;
	variable BarrierQ_TMGa_inject_flow = 37.3*calib_factor_1;
	variable TMGa_2_Total_flow = 400;
	variable TMGa_2_conc = 25889;
	variable BarrierQ_Time = 8/0.592;



##	m33-QW Q (1430nm PL, 0.15% Compressive) - TMGa_1,AsH3_1
	variable m33Q_AsH3_flow = 89.9;
	variable AsH3_1_Total_flow = 500;
	variable m33Q_PH3_flow = 800;
	variable m33Q_TMIn_flow = 696*calib_factor_1;
	variable m33Q_TMGa_1_flow = 17.1*calib_factor_1; #corrected from 17.4
	variable m33Q_TMGa_1_Total_flow = 200;
	variable m33Q_TMGa_1_conc = 9800*calib_factor_1;
	variable m33Q_Time = 9.5/0.722;


##	Q1.3 using TMGa-2 and AsH3-1

	variable ES_AsH3_flow = 42.2;
	variable AsH3_1_ES_Total_flow = 500;
	variable ES_PH3_flow = 800;
	variable ES_TMGa_source_flow = 100;
	variable ES_TMGa_dilute_flow = 300;
	variable ES_TMGa_inject_flow = 67.5*calib_factor_1;
	variable TMGa_2_ES_Total_flow = 400;
	variable TMGa_2_ES_conc = 25889;

	variable ES_Time=10/0.667;






####################################################################


####################################################################
##      RECIPE CORE
####################################################################


layer {

   Basestate;
                DORCheck;

          until TimerN2PurgeActive == off;


		Ini_1Macro;

	0.1 "set stuff",
		StepCode=1,
		DummyHyd_2.source follow AsH3_2.inject,
		DummyHyd_2.run follow [1-AsH3_2.run],
		DummyHyd_1.source = 500,
		DummyHyd_1.run follow [1-AsH3_1.run],
		DummyMO1_1.source follow [m33Q_TMGa_1_Total_flow],
		DummyMO1_1.run follow [1-TMGa_1.run],
		DummyMO1_2.source follow TMGa_2.inject,
		DummyMO1_2.run follow [1-TMGa_2.run],
		DummyDop_1.source follow Si2H6_1.inject,
		DummyDop_1.run follow [1-Si2H6_1.run],
		PH3_1.source = PH3_Stabilization,
		PH3_1.push = 1000-PH3_Stabilization;

        60      "Ramping to Process Flows",
                Optical.purge to (Growth_Total_Flow  / 240),
                Liner.purge to (Growth_Total_Flow  / 6),
                Heater.purge to (Growth_Total_Flow  / 3),
                RunHyd1.feed1 follow (Growth_Total_Flow/2-PH3_1.source-PH3_1.push-DummyHyd_2.source-DummyHyd_1.source),
                RunMO1.feed1 follow (Growth_Total_Flow/6-DummyMO1_1.source-DummyMO1_2.source-1500*[TMIn_1.run]-1500*[TMIn_2.run]),
                RunMO1.feed2 to (Growth_Total_Flow  / 6),
                RunDop1.feed1 follow (Growth_Total_Flow / 6-DummyDop_1.source),
                RunHyd1.vent to  (Growth_Total_Flow  / 16),
                RunMO1.vent to  (Growth_Total_Flow  / 32),
                RunDop1.vent to  (Growth_Total_Flow  / 32);




Ini_2Macro;



DesorbMacro;



###	InP  buffer
	[(Desorb_Temp-Buffer_Temp)/CoolRate] "Cool down for InP buffer",
	Heater.temp to Buffer_Temp,
	HeaterzoneA.factor to Buffer_ZoneA-6,
                HeaterzoneB.factor to Buffer_ZoneB-4,
                HeaterzoneC.factor = Buffer_ZoneC+5,
	TMIn_1.line = on,
	TMIn_2.line = on;

	90	"Wait for stability",
		HeaterzoneA.factor to Buffer_ZoneA in 60,
              		HeaterzoneB.factor to Buffer_ZoneB in 60,
		HeaterzoneC.factor to Buffer_ZoneC in 30,
		TMIn_1.source to Buffer_TMIn_1_flow in 45,
		TMIn_1.push to Buffer_TMIn_Total_flow-Buffer_TMIn_1_flow in 45,
		TMIn_2.source to Buffer_TMIn_2_flow in 45,
		TMIn_2.push to Buffer_TMIn_Total_flow-Buffer_TMIn_2_flow in 45,
		TMIn_1.totalflow = 1500,
		TMIn_1.setconc = TMIn_1_conc,
		TMIn_2.totalflow = 1500,
		TMIn_2.setconc = TMIn_2_conc,
		PH3_1.source to Buffer_PH3_flow,
		PH3_1.push to 100,
		Si2H6_1.line= on,
		Si2H6_1.source = Si_source_1,
		Si2H6_1.dilute = Si_dilute_1,
		Si2H6_1.inject = Si_inject_1;


	60	"Epison on",
		TMIn_1.control = on,
		TMIn_2.control = on;

	[InP_Si_Time_1-45] "N+ InP",
		TMIn_1.run = on,
		TMIn_2.run = on,
		Si2H6_1.run=on;

	30 "reset SiH6 source flow",
		Si2H6_1.source = Si_source_2,
		Si2H6_1.dilute = Si_dilute_2;

	15 "reset SiH6 inject flow",
		Si2H6_1.source = Si_source_2,
		Si2H6_1.dilute = Si_dilute_2,
		Si2H6_1.inject = Si_inject_2;

	[InP_Si_Time_2] "N InP",
		TMIn_1.run = on,
		TMIn_2.run = on,
		Si2H6_1.run=on;

	30 "pause growth and reset SiH6 flow",
		TMIn_1.run = off,
		TMIn_2.run = off,
		Si2H6_1.run=off,
		Si2H6_1.source = Si_source_3,
		Si2H6_1.dilute = Si_dilute_3,
		Si2H6_1.inject = Si_inject_3;

	[InP_Si_Time_3-30] "lower n clad 1",
		TMIn_1.run = on,
		TMIn_2.run = on,
		Si2H6_1.run=on;

	[30] "reset SiH6",
		Si2H6_1.inject = Si_inject_4;

	[InP_Si_Time_4-60] "lower n clad 2",
		TMIn_1.run = on,
		TMIn_2.run = on,
		Si2H6_1.run=on,

		AsH3_2.line = open,
		AsH3_2.source to BarrierQ_AsH3_2_source_flow,
		AsH3_2.dilute to BarrierQ_AsH3_2_dilute_flow,
		AsH3_2.inject to BarrierQ_AsH3_2_inject_flow,
		TMGa_2.line = open,
		TMGa_2.source to BarrierQ_TMGa_source_flow in 30,
		TMGa_2.dilute to BarrierQ_TMGa_dilute_flow in 30,
		TMGa_2.inject to BarrierQ_TMGa_inject_flow,
		TMGa_2.setconc = TMGa_2_conc,
		TMGa_2.totalflow = TMGa_2_Total_flow,

		AsH3_1.line = open,
		AsH3_1.source to m33Q_AsH3_flow,
		AsH3_1.push to AsH3_1_Total_flow-m33Q_AsH3_flow,
		TMGa_1.line = open,
		TMGa_1.source to m33Q_TMGa_1_flow in 30,
		TMGa_1.push to m33Q_TMGa_1_Total_flow - m33Q_TMGa_1_flow in 30,
		TMGa_1.setconc = m33Q_TMGa_1_conc,
		TMGa_1.totalflow = m33Q_TMGa_1_Total_flow;



	60	"TMGa_2 Epison control active",
		TMGa_1.control = on,
		TMGa_2.control = on;

	[InP_Spacer_bottom_Time] "lower Spacer",
		TMIn_1.run = on,
		TMIn_2.run= off,
		TMIn_2.line=off,
		TMIn_2.control = off,
		Si2H6_1.run=off,
		Si2H6_1.line=off;



	[BarrierQ_Time] "Barrier",
		TMIn_1.run= on,
		TMGa_2.run = on,
		AsH3_2.run = on;


#LOOP

loop 33 {

	[m33Q_Time]
		TMGa_1.run = on,
		AsH3_1.run = on,
		AsH3_2.run = off,
		TMGa_2.run = off;



	[BarrierQ_Time]

		TMGa_1.run = off,
		AsH3_1.run = off,
		AsH3_2.run = on,
		TMGa_2.run = on;

}

	[InP_Spacer_1_Time-60] "lower Spacer",
		TMIn_1.run = on,

		TMGa_1.line = off,
		TMGa_1.run = off,
		TMGa_1.control = off,
		AsH3_2.line = off,
		AsH3_2.run = off,

		TMGa_2.run = off,
		TMGa_2.control = off,
		AsH3_1.run = off,

		AsH3_1.line = on,
		AsH3_1.source to ES_AsH3_flow,
		AsH3_1.push to AsH3_1_ES_Total_flow-ES_AsH3_flow,
		TMGa_2.line = on,
		TMGa_2.source to ES_TMGa_source_flow in 30,
		TMGa_2.dilute to ES_TMGa_dilute_flow in 30,
		TMGa_2.inject to ES_TMGa_inject_flow,
		TMGa_2.setconc = TMGa_2_ES_conc,
		TMGa_2.totalflow = TMGa_2_ES_Total_flow;


	60	"TMGa_2 Epison control active",
		TMGa_2.control = on;


	[ES_Time] "diffusion barrier 1",
		TMIn_1.run= on,
		TMGa_2.run = on,
		AsH3_1.run = on;

	[InP_Spacer_2_Time] "diffusion barrier 1",
		TMIn_1.run= on,
		TMGa_2.run = off,
		AsH3_1.run = off;

	[ES_Time] "diffusion barrier 2",
		TMIn_1.run= on,
		TMGa_2.run = on,
		AsH3_1.run = on;

	[InP_clad_Time] "InP clad",
		TMIn_1.run= on,
		TMGa_2.run = off,
		TMGa_2.line = off,
		TMGa_2.control = off,
		AsH3_1.run = off,
		AsH3_1.line = off;




	0.1 "turn off",
		TMIn_1.run= off,
		TMIn_1.line=off,
		TMIn_1.control = off;



    EndMacro;



    Basestate;

    Pressure_Control_1200;


}
####################################################################

