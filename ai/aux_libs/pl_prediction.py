import re
import os
import numpy as np
import pandas as pd
from glob import glob
from joblib import load

from ai.aux_libs.recipe_functions import recipe_cleaner,recipe_parser
from ai.aux_libs.epitt_functions2 import epitt_cleaner, epitt_parser

from pandas import read_pickle, DataFrame

from ai.aux_libs import *

pd.options.mode.chained_assignment = None  #

#Load models first 


#path_folder_test = '/Users/ecastillo/Documents/GestaLabs/photonics-prot/ai/test_data'


def predict_pl_with_files_new(path_folder_test):
   
    """
    This function predicts pl by Recipe and EpiTT files with NO aluminum variables
    Parameters:
    path_folder_test: str
    The path where the Recipe and EpiTT subfolder are living
   
   
   
    """
   
    model = pd.read_pickle("ai/aux_libs/joblib/model.pkl")
    pipeline_pre = pd.read_pickle("ai/aux_libs/joblib/pipeline_pre.pkl")
    features = pd.read_pickle("ai/aux_libs/joblib/features.pkl")
    scaler_target = pd.read_pickle("ai/aux_libs/joblib/scaler_target.pkl")
    ls_folder = os.listdir(path_folder_test)
    for folder in ls_folder:
        if "recipe" in folder.lower():
            path_recipe = folder
        if "epitt" in folder.lower():
            path_epitt = folder
    ls_recipe = glob(os.path.join(path_folder_test, path_recipe, "*epi"))[0]
    ls_epitt = glob(os.path.join(path_folder_test, path_epitt, "*dat"))
   
   
    recipe_data = recipe_cleaner(recipe_parser(ls_recipe))
    recipe_df = pd.DataFrame(recipe_data, index = [0])
    epitt_df = pd.DataFrame([epitt_cleaner(epitt_parser(file)) for file in ls_epitt])
    epitt_df.drop_duplicates(subset = ["wafer"], inplace = True)
   
    df_epitt_recipe = pd.DataFrame()
    for index, row in epitt_df.iterrows():
        try:
            run = row["run"]
            wafer = row["wafer"]
            recipe_time = recipe_df.loc[recipe_df["run"]==run, "epitt_time"].values[0]
            df_time = pd.DataFrame(row["nested_data"])
            df_time = df_time[df_time["begin"]<=recipe_time]
            for i in range(2, 25):
                df_time[f"pyro_temp_ma_{i}"] = df_time["pyro_temp"].rolling(i).mean()
            df_time = df_time.drop(columns=["begin", "pyro_temp"]).tail(1)
            df_epitt_recipe.loc[index, "run"] = run
            df_epitt_recipe.loc[index, "wafer"] = wafer
            for col in df_time.columns:
                df_epitt_recipe.loc[index, col] = df_time[col].tolist()
        except Exception as e:
            continue
    df_epitt = df_epitt_recipe.groupby(["run", "wafer"], as_index=False).mean()

    df_test = epitt_df.merge(recipe_df, on="run", how="left")
    aux = pd.DataFrame([], columns = features)
    cols = df_test.columns[(df_test.columns.isin(aux.columns))]
   
    missing_cols = list(set(features).difference(set(cols)))
    data = df_test[cols]
    for missing in missing_cols:
        data.loc[:,missing] = np.nan
    data.loc[data["ash3_1__source"].isna() & (~data["ash3_2__source"].isna()), "ash3_1__source"] = 0
    data.loc[(~data["ash3_1__source"].isna()) & (data["ash3_2__source"].isna()), "ash3_2__source"] = 0
    data.loc[data["tmga_1__setconc"].isna() & (~data["tmga_2__setconc"].isna()), "tmga_1__setconc"] = 0
    data.loc[(~data["tmga_1__setconc"].isna()) & (data["tmga_2__setconc"].isna()), "tmga_2__setconc"] = 0    
    print(data.join(df_test[["run","wafer"]]))

    arch_que_va_descargar = data.join(df_test[["run","wafer"]]) ##<----archivo que descargaría el usuario 

    Xs = pd.DataFrame(data=pipeline_pre.transform(data), columns=features)
    result = pd.DataFrame(scaler_target.inverse_transform(model.predict(Xs[features]).reshape(-1,1)).ravel()).rename(columns = {0:"PL Prediction"})
    result = result.join(df_test[["run","wafer"]])
    prediction = result[["run","wafer", "PL Prediction"]]
   
    return prediction,arch_que_va_descargar


def csv_NO_Al(path_folder_test):
    
    ls_folder = os.listdir(path_folder_test)
    for folder in ls_folder:
        if "recipe" in folder.lower():
            path_recipe = folder
        if "epitt" in folder.lower(): 
            path_epitt = folder
    ls_recipe = glob(os.path.join(path_folder_test, path_recipe, "*epi"))[0]
    ls_epitt = glob(os.path.join(path_folder_test, path_epitt, "*dat"))
    
    
    recipe_data = recipe_cleaner(recipe_parser(ls_recipe))
    recipe_df = pd.DataFrame(recipe_data, index = [0])
    epitt_df = pd.DataFrame([epitt_cleaner(epitt_parser(file)) for file in ls_epitt])
    epitt_df.drop_duplicates(subset = ["wafer"], inplace = True)
    
    df_epitt_recipe = pd.DataFrame()
    for index, row in epitt_df.iterrows():
        try:
            run = row["run"]
            wafer = row["wafer"]
            recipe_time = recipe_df.loc[recipe_df["run"]==run, "epitt_time"].values[0]
            df_time = pd.DataFrame(row["nested_data"])
            df_time = df_time[df_time["begin"]<=recipe_time]
            for i in range(2, 25):
                df_time[f"pyro_temp_ma_{i}"] = df_time["pyro_temp"].rolling(i).mean()
            df_time = df_time.drop(columns=["begin", "pyro_temp"]).tail(1)
            df_epitt_recipe.loc[index, "run"] = run
            df_epitt_recipe.loc[index, "wafer"] = wafer
            for col in df_time.columns:
                df_epitt_recipe.loc[index, col] = df_time[col].tolist()
        except Exception as e:
            continue
    df_epitt = df_epitt_recipe.groupby(["run", "wafer"], as_index=False).mean()

    df_test = epitt_df.merge(recipe_df, on="run", how="left")
    features = [
                 'ash3_1__source',
                 'tega_1__setconc',
                 'ash3_2__source',
                 'pyro_temp_ma_4',
                 'ash3_2__dilute',
                 'tmga_2__setconc',
                 'tmga_1__setconc',
                 'ash3_2__inject',
                 'tmga_2__inject',
                 'thickness' ]
    df_test = df_test.merge(df_epitt[["run","wafer","pyro_temp_ma_4"]], on=["run","wafer"], how="left")
    aux = pd.DataFrame([], columns = features)
    cols = df_test.columns[(df_test.columns.isin(aux.columns))]
    missing_cols = list(set(features).difference(set(cols)))
    data = df_test[cols]
    for missing in missing_cols:
        data.loc[:,missing] = np.nan
   
    return data




# def predict_pl_variables(dc):   
#     """
#     Parameters
#     dc:Dict 
#     Dictionary with the input values by the user 
    
#     Returns:
#     result: np.array 
#     Prediction 
    
    
#     """
#     imputer = load('ai/aux_libs/joblib/best_imputer.joblib')
#     X_scaler = load("ai/aux_libs/joblib/X_scaler.joblib")
#     y_scaler = load("ai/aux_libs/joblib/y_scaler.joblib")
#     model = load("ai/aux_libs/joblib/XGBRegressor_best_model.joblib")

    
#     data = pd.DataFrame(dc, index = [0])
#     Xi = pd.DataFrame(imputer.transform(data), columns = data.columns)
#     Xs = pd.DataFrame(X_scaler.transform(Xi),columns=data.columns)
#     result = y_scaler.inverse_transform(model.predict(Xs).reshape(-1,1)).ravel()
#     return result[0]


def predict_pl_variables(dc):
    """
    Parameters
    dc:Dict 
    Dictionary with the input values by the user 
    
    Returns:
    result: np.array 
    Prediction 
    
    
    """
    pipeline_pre = read_pickle("ai/aux_libs/joblib/pipeline_pre.pkl")
    model = read_pickle("ai/aux_libs/joblib/model.pkl")
    features = read_pickle("ai/aux_libs/joblib/features.pkl")
    scaler_target = read_pickle("ai/aux_libs/joblib/scaler_target.pkl")
    X = DataFrame([dc])
    X.loc[
        X["ash3_1__source"].isna() & (~X["ash3_2__source"].isna()), "ash3_1__source"
    ] = 0
    X.loc[
        (~X["ash3_1__source"].isna()) & (X["ash3_2__source"].isna()), "ash3_2__source"
    ] = 0
    X.loc[
        X["tmga_1__setconc"].isna() & (~X["tmga_2__setconc"].isna()), "tmga_1__setconc"
    ] = 0
    X.loc[
        (~X["tmga_1__setconc"].isna()) & (X["tmga_2__setconc"].isna()),
        "tmga_2__setconc",
    ] = 0
    X = DataFrame(
        data=pipeline_pre.transform(X),
        columns=features,
    )
    result = scaler_target.inverse_transform(
        model.predict(X[features]).reshape(-1, 1)
    ).ravel()
    return result




##
#predict_pl_with_files(path_folder_test)