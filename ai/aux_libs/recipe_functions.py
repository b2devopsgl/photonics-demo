import os
import re
import sys
import pandas as pd
from glob import glob
from functools import reduce
from sympy.parsing.sympy_parser import parse_expr
import numpy as np

def is_digit(text):
    return text.strip().replace(".", "").isdigit()

def clean_equation(equation):
    index = equation.find("#")
    equation = equation[:index] if index > 0 else equation
    return equation.replace("[", "(").replace("]", ")").replace(";", "").replace(",", "") + " "

def get_elements(equation):
    equation = clean_equation(equation)
    index = 0
    end = 0
    ls_elements = []
    for i, c in enumerate(equation):
        if c == "(":
            index+=1
            continue
        if c in [" ", "/", "*", "+", ")", "-"]:
            end = i
            ls_elements.append(equation[index:end])
            index = i+1
    return ls_elements

def evaluate_expression(expression, dc_values):
    try:
        formula = clean_equation(expression)
        ls_elements = [x for x in get_elements(formula) if len(x)>0 and not is_digit(x)]
        if all(element in dc_values for element in ls_elements):
            ls_replace = [x for x in ls_elements if x in dc_values]
            formula = reduce(lambda string, variable: string.replace(variable, str(dc_values[variable])), ls_replace, formula)
            return float(parse_expr(formula.strip()))
        else:
            return np.nan
    except:
        return np.nan
    return ls_elements



def recipe_parser(file_path):
    try: 
        
        #file_path= "repo_photonics/recipe/C1551-1st OG-Product_2017_12_1_7_25_48.epi"
        with open(file=file_path, mode="r", encoding="utf-16") as f:
            text = "\n".join([x.strip() for x in f.read().split("\n")])
        dc_layer_len = []
        dc_variables = {}
        # dc_variables = dict(map(lambda x: x.split("="), re.findall("variable (.*?);", text)))

        v_index = text.find("variable")

        c_index = text[v_index:].find("##")

        variables_block = text[v_index:v_index+c_index]

        dc_variables = {**dc_variables, **dict(map(lambda x: x.split("="), re.findall("variable (.*?);", variables_block)))}

        layer_index = text.find("layer {")

        variables_block = text[v_index+c_index:layer_index]

        ls_index = [x.start() for x in re.finditer(r"^##", variables_block, re.MULTILINE)]

        ls_blocks = [variables_block[start: end] for start, end in zip(ls_index, ls_index[1:])]   
        flag_block = False
        block_active = True #Indica si el bloque anterior contuvo variables
        last_block_pl = None 
        #For getting the longest wavelength 
        dc_layer = {}
        for block in ls_blocks:    
            lines = block.splitlines()
            #print("block------------>", block)
            #print(lines, len(lines))
            #print(lines[0])
            if len(lines) <= 1 or sum(map(len,lines[1:])) == 0:
                continue
            if "mqw" in lines[0].lower():
                pl = 5000
                dc_layer[pl] = dict(map(lambda x: x.split("="), re.findall("variable (.*?);", block)))
                break
                
            #print(lines[0])
            if "InGaAs" in lines[0] or "InAlAs" in lines[0] or "097Q" in lines[0] or "Q097" in lines[0]: 
                continue 
            if len(re.findall(r"Q\d+\.\d+|Q\d+", lines[0])) == 0 and block_active  and not "well" in lines[0].lower() and len(re.findall(r"\d+(?=[\s*|\.\d+]*nm)", lines[0])) == 0:
                #print("paso por aca")
                dc_variables = {**dc_variables, **dict(map(lambda x: x.split("="), re.findall("variable (.*?);", block)))}
            else: 
                if "well" in lines[0].lower():
                    #print("HOLA :)")
                    for pl_regex in [r" \d{3,4}[\s\.]", r"\d+\.\d+Q|\d+Q", r"Q\d+\.\d+|Q\d+", r"\d{3,4}(?=[\s*|\.\d+]*nm)"]:
                        ls_layer_size = re.findall(pl_regex, lines[0])
                        if len(ls_layer_size) > 0:
                            pl = float(ls_layer_size[0].replace("Q", "").replace("nm", "").strip())
                            dc_layer[pl] = dict(map(lambda x: x.split("="), re.findall("variable (.*?);", block)))
                            break
                        else: 
                            continue
                    continue 
                if len(lines) < 3:
                    if "nm" in lines[0]:
                        pl = float(re.findall(r"\d+(?=[\s*|\.\d+]*nm)", lines[0])[0])
                        pl = pl*10**((3 - int(np.log10(pl)))) if pl*10**((3 - int(np.log10(pl)))) < 2000 else float(pl)
                        last_block_pl = pl
                        block_active = False
                        dc_layer[pl] = dict(map(lambda x: x.split("="), re.findall("variable (.*?);", block)))
                        continue
                    else:
                        pl = float(re.findall(r"Q\d+\.\d+|Q\d+", lines[0])[0][1:])
                        pl = pl*10**((3 - int(np.log10(pl)))) if pl*10**((3 - int(np.log10(pl)))) < 2000 else float(pl)
                        last_block_pl = pl
                        block_active = False
                        dc_layer[pl] = dict(map(lambda x: x.split("="), re.findall("variable (.*?);", block)))
                        continue

                if "nm" in lines[0] and block_active:
                    pl_found = re.findall(r"\d+(?=[\s*|\.\d+]*nm)", lines[0])
                    if len(pl_found) > 2:
                        pl = max([float(pl) for pl in (re.findall(r"\d+(?=[\s*|\.\d+]*nm)",lines[0] ))])
                        pl = pl*10**((3 - int(np.log10(pl)))) if pl*10**((3 - int(np.log10(pl)))) < 2000 else float(pl)
                    else:
                        print(lines[0])
                        pl = float(re.findall(r"\d+(?=[\s*|\.\d+]*nm)", lines[0])[0]) ###considerar punto
                        pl = pl*10**((3 - int(np.log10(pl)))) if pl*10**((3 - int(np.log10(pl)))) < 2000 else float(pl)
               # dc_layer[pl] = dict(map(lambda x: x.split("="), re.findall("variable (.*?);", block)))
                else:

                    if not block_active:
                        #print("hola")
                        pl = last_block_pl
                    else:
                        #print("Hola")
                        #print(lines[0])
                        pl = float(re.findall(r"Q\d+\.\d+|Q\d+",lines[0])[0][1:])
                        pl = pl*10**((3 - int(np.log10(pl)))) if pl*10**((3 - int(np.log10(pl)))) < 2000 else float(pl)

                dc_layer[pl] = dict(map(lambda x: x.split("="), re.findall("variable (.*?);", block)))
                block_active = True #Indica si el bloque anterior contuvo variables
                last_block_pl = None
        #print("dc_variables es este ----> ", dc_variables)
        dc_layer_len.append(len(dc_layer))
        #print("Este es el dc_layer----->",dc_layer)
        if not len(dc_layer) == 0:
            correct_layer = dc_layer[max(dc_layer.keys())]
            #print("Esta es la correct layer --------> ", correct_layer)
            time_variables = [x for x in correct_layer.keys() if x.lower().endswith("_time")]
            correct_layer["thickness"] = correct_layer[time_variables[0]].split("/")[0] if len(time_variables) > 0 else "-1000"
            dc_variables = {**dc_variables, **correct_layer}
        
        dc_variables = {key.strip(): float(value) if is_digit(value) else value for key, value in dc_variables.items()}
        
        dc_variables = {**dc_variables, **{"on": 1, "off": 0, "open": 1, "close": 0}}
        #print(dc_variables)
        for key, value in dc_variables.items():
            if isinstance(value, str):
                ls_elements = get_elements(value)
                ls_string_elements = [x for x in ls_elements if not is_digit(x)]
                ls_replace = [x for x in ls_string_elements if x in dc_variables]
                for variable in ls_replace:
                    value = value.replace(variable, str(dc_variables[variable]))
                dc_variables[key] = float(parse_expr(value))
            else:
                continue
        #print(dc_variables)
        equations = [x.strip() for x in re.findall(r"^layer[\s\S]+^}", text, re.MULTILINE)[0].split("\n")[1:-1]]
        ls_times = []
        for equation in equations:
            if "loop" in equation:
                break
            time_step = re.findall(r"(^\d.*?|^\[.*?\])\s", equation)
            if len(time_step) > 0:
                ls_times.append(evaluate_expression(time_step[0], dc_variables))
        epitt_time = np.nansum(ls_times)
        equations = [x for x in equations if len(x) > 0 and not is_digit(x[0]) and not x[0]=="#" and '"' not in x]
        equations = [line.strip() for line in equations if any([substring in line for substring in ("=", " follow ", " to ")]) and "==" not in line]
        dc_follow = {}
        dc_formulas = {}
        for equation in equations:
            if " follow " in  equation:
                feature, expression = equation.split(" follow ")
                expression = expression.strip()
                elements = [x for x in get_elements(expression) if not is_digit(x) and len(x) > 0]
                for eq_element in elements:
                    if eq_element in dc_follow:
                        dc_follow[eq_element] += [{"feature": feature, "equation": expression}]
                    else:
                        dc_follow[eq_element] = [{"feature": feature, "equation": expression}]
                formula = clean_equation(expression)
                ls_elements = [x for x in get_elements(formula) if len(x)>0 and not is_digit(x)]
                if all(element in {**dc_formulas, **dc_variables} for element in ls_elements):
                    ls_string_elements = [x for x in ls_elements if not is_digit(x)]
                    ls_replace = [x for x in ls_string_elements if x in {**dc_formulas, **dc_variables}]
                    for variable in ls_replace:
                        if variable in dc_formulas:
                            formula = formula.replace(variable, str(dc_formulas[variable]))
                        if variable in dc_variables:
                            formula = formula.replace(variable, str(dc_variables[variable]))
                    feature = feature.strip()
                    dc_formulas[feature] = float(parse_expr(formula))
        for equation in equations:
            eq = clean_equation(equation)
            if "=" in eq:
                feature, formula = eq.split("=")
                formula = formula.strip()
                ls_elements = [x for x in get_elements(formula) if len(x)>0]
                ls_string_elements = [x for x in ls_elements if not is_digit(x)]
                ls_replace = [x for x in ls_string_elements if x in dc_variables]
                for variable in ls_replace:
                    formula = formula.replace(variable, str(dc_variables[variable]))
                feature = feature.strip()
                if all(element in {**dc_formulas, **dc_variables} for element in ls_elements):
                    try:
                        dc_formulas[feature] = float(parse_expr(formula.strip()))
                    except:
                        continue
                if len(ls_elements) == 1 and len(ls_string_elements) == 0:
                    if feature not in dc_formulas.keys(): 
                        dc_formulas[feature] = float(formula)        
            if " to " in eq:
                feature, formula = eq.split(" to ")
                if " in " in formula:
                    formula = formula[:formula.find(" in ")+1]
                formula = formula.strip()
                formula = clean_equation(formula)
                ls_elements = [x for x in get_elements(formula) if len(x)>0 and not is_digit(x)]
                ls_string_elements = [x for x in ls_elements if not is_digit(x)]
                ls_replace = [x for x in ls_string_elements if x in dc_variables]

        #                 print(ls_elements)
                for variable in ls_replace:
                    formula = formula.replace(variable, str(dc_variables[variable]))
                feature = feature.strip()
                #print(f"equation: {eq} feature: {feature}. formula: {formula}")
                if all(element in {**dc_formulas, **dc_variables} for element in ls_elements):
                    try:
                        dc_formulas[feature] = float(parse_expr(formula))
                    except:
                        pass
            for ft_follow in dc_follow:
                for eq_follow in dc_follow[ft_follow]:
                    feat, formula = eq_follow["feature"], eq_follow["equation"]
                    formula = formula.strip()
                    formula = clean_equation(formula)
                    ls_elements = [x for x in get_elements(formula) if len(x)>0 and not is_digit(x)]
                    if all(element in {**dc_formulas, **dc_variables} for element in ls_elements):
                        ls_string_elements = [x for x in ls_elements if not is_digit(x)]
                        ls_replace = [x for x in ls_string_elements if x in {**dc_formulas, **dc_variables}]
                        for variable in ls_replace:
                            if variable in dc_formulas:
                                formula = formula.replace(variable, str(dc_formulas[variable]))
                            if variable in dc_variables:
                                formula = formula.replace(variable, str(dc_variables[variable]))
                        feat = feat.strip()
                        dc_formulas[feat] = float(parse_expr(formula))
                    else:
                        continue
        run = re.findall("C\d{2,4}", file_path)
        run = run.pop() if run else None
        ls_date = re.findall("\d{4}_\d{1,2}_\d{1,2}_\d{1,2}_\d{1,2}_\d{1,2}", file_path)
        date = ls_date.pop() if ls_date else None
        year, month, day, hour, minute, second = date.split("_") if date else [None]*6
        date = "{}/{}/{}T{}:{}:{}".format(year, month.zfill(2), day.zfill(2), hour.zfill(2), minute.zfill(2), second.zfill(2)) if year else None
        dc_formulas = {**{"filename": os.path.basename(file_path), "run": run, "date": date}, **dc_formulas}
        dc_formulas["thickness"] = dc_variables["thickness"] if "thickness" in dc_variables.keys() else np.nan
        dc_formulas["thickness"] = dc_formulas["thickness"] if dc_formulas["thickness"] != -1000 else np.nan
        dc_formulas["epitt_time"] = epitt_time
        return dc_formulas 
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        print(file_path, str(e))
        return {} 

def recipe_cleaner(data):
    data = {key.split()[-1]: value for key, value in data.items()}
    data = {key.lower().replace(".", "__"): value for key, value in data.items()}
    return data        