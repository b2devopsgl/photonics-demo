import re
import os
import numpy as np
import pandas as pd
from glob import glob
from joblib import load
import sys, getopt

from ai.aux_libs.recipe_functions import recipe_cleaner,recipe_parser
from ai.aux_libs.epitt_functions2 import epitt_cleaner, epitt_parser

pd.options.mode.chained_assignment = None  #

def predict_pl_with_files_Al(path_folder_test):
    
    """
    This function predicts pl by Recipe and EpiTT files with aluminum variables 
    
    Parameters:
    path_folder_test: str
    The path where the Recipe and EpiTT subfolder are living 
    
    
    
    """
    
    
    ls_folder = os.listdir(path_folder_test)
    for folder in ls_folder:
        if "recipe" in folder.lower():
            path_recipe = folder
        if "epitt" in folder.lower(): 
            path_epitt = folder
    ls_recipe = glob(os.path.join(path_folder_test, path_recipe, "*epi"))[0]
    ls_epitt = glob(os.path.join(path_folder_test, path_epitt, "*dat"))
    
    
    recipe_data = recipe_cleaner(recipe_parser(ls_recipe))
    recipe_df = pd.DataFrame(recipe_data, index = [0])
    epitt_df = pd.DataFrame([epitt_cleaner(epitt_parser(file)) for file in ls_epitt])
    epitt_df.drop_duplicates(subset = ["wafer"], inplace = True)
    
    df_epitt_recipe = pd.DataFrame()
    for index, row in epitt_df.iterrows():
        try:
            run = row["run"]
            wafer = row["wafer"]
            recipe_time = recipe_df.loc[recipe_df["run"]==run, "epitt_time"].values[0]
            df_time = pd.DataFrame(row["nested_data"])
            df_time = df_time[df_time["begin"]<=recipe_time]
            for i in range(2, 25):
                df_time[f"pyro_temp_ma_{i}"] = df_time["pyro_temp"].rolling(i).mean()
            df_time = df_time.drop(columns=["begin", "pyro_temp"]).tail(1)
            df_epitt_recipe.loc[index, "run"] = run
            df_epitt_recipe.loc[index, "wafer"] = wafer
            for col in df_time.columns:
                df_epitt_recipe.loc[index, col] = df_time[col].tolist()
        except Exception as e:
            continue
    df_epitt = df_epitt_recipe.groupby(["run", "wafer"], as_index=False).mean()

    df_test = epitt_df.merge(recipe_df, on="run", how="left")
    features = ["ash3_1__source",
                "pyro_temp_ma_4",
                "tmal_1__setconc",
                "tmal_2__setconc",
                "tmal_2__inject",
                "thickness",
                "tmga_2__setconc",
                "tmga_1__setconc"]
    aux = pd.DataFrame([], columns = features)
    cols = df_test.columns[(df_test.columns.isin(aux.columns))]
    missing_cols = list(set(features).difference(set(cols)))
    data = df_test[cols]
    for missing in missing_cols:
        data.loc[:,missing] = np.nan
    imputer = load('ai/aux_libs/joblib/best_imputer_al.joblib')
    X_scaler = load("ai/aux_libs/joblib/X_scaler_al.joblib")
    y_scaler = load("ai/aux_libs/joblib/y_scaler_al.joblib")
    model = load("ai/aux_libs/joblib/XGBRegressor_best_model_al.joblib")
    Xi = pd.DataFrame(imputer.transform(data), columns = data.columns)
    Xs = pd.DataFrame(X_scaler.transform(Xi),columns=data.columns)
    result = pd.DataFrame(y_scaler.inverse_transform(model.predict(Xs).reshape(-1,1)).ravel()).rename(columns = {0:"PL Prediction"})
    result = result.join(df_test[["run","wafer"]])
    prediction = result[["run","wafer", "PL Prediction"]]
    
    print(prediction)
    return prediction



def csv_Al(path_folder_test):
    
    ls_folder = os.listdir(path_folder_test)
    for folder in ls_folder:
        if "recipe" in folder.lower():
            path_recipe = folder
        if "epitt" in folder.lower(): 
            path_epitt = folder
    ls_recipe = glob(os.path.join(path_folder_test, path_recipe, "*epi"))[0]
    ls_epitt = glob(os.path.join(path_folder_test, path_epitt, "*dat"))
    
    
    recipe_data = recipe_cleaner(recipe_parser(ls_recipe))
    recipe_df = pd.DataFrame(recipe_data, index = [0])
    epitt_df = pd.DataFrame([epitt_cleaner(epitt_parser(file)) for file in ls_epitt])
    epitt_df.drop_duplicates(subset = ["wafer"], inplace = True)
    
    df_epitt_recipe = pd.DataFrame()
    for index, row in epitt_df.iterrows():
        try:
            run = row["run"]
            wafer = row["wafer"]
            recipe_time = recipe_df.loc[recipe_df["run"]==run, "epitt_time"].values[0]
            df_time = pd.DataFrame(row["nested_data"])
            df_time = df_time[df_time["begin"]<=recipe_time]
            for i in range(2, 25):
                df_time[f"pyro_temp_ma_{i}"] = df_time["pyro_temp"].rolling(i).mean()
            df_time = df_time.drop(columns=["begin", "pyro_temp"]).tail(1)
            df_epitt_recipe.loc[index, "run"] = run
            df_epitt_recipe.loc[index, "wafer"] = wafer
            for col in df_time.columns:
                df_epitt_recipe.loc[index, col] = df_time[col].tolist()
        except Exception as e:
            continue
    df_epitt = df_epitt_recipe.groupby(["run", "wafer"], as_index=False).mean()

    df_test = epitt_df.merge(recipe_df, on="run", how="left")
    features = ["ash3_1__source",
                "pyro_temp_ma_4",
                "tmal_1__setconc",
                "tmal_2__setconc",
                "tmal_2__inject",
                "thickness",
                "tmga_2__setconc",
                "tmga_1__setconc"]
    aux = pd.DataFrame([], columns = features)
    cols = df_test.columns[(df_test.columns.isin(aux.columns))]
    missing_cols = list(set(features).difference(set(cols)))
    data = df_test[cols]

    for missing in missing_cols:
        data.loc[:,missing] = np.nan
  
    return data


def predict_pl_variables_Al(dc):   
    """
    Parameters
    dc:Dict 
    Dictionary with the input values by the user 
    
    Returns:
    result: np.array 
    Prediction 
    
    
    """
        
    imputer = load('ai/aux_libs/joblib/best_imputer_al.joblib')
    X_scaler = load("ai/aux_libs/joblib/X_scaler_al.joblib")
    y_scaler = load("ai/aux_libs/joblib/y_scaler_al.joblib")
    model = load("ai/aux_libs/joblib/XGBRegressor_best_model_al.joblib")
    data = pd.DataFrame(dc, index = [0])
    Xi = pd.DataFrame(imputer.transform(data), columns = data.columns)
    Xs = pd.DataFrame(X_scaler.transform(Xi),columns=data.columns)
    result = y_scaler.inverse_transform(model.predict(Xs).reshape(-1,1)).ravel()
    print(result)
    return result

