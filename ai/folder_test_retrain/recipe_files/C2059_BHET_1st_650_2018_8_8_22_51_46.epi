 #Bubblers: TMIn_1 and 2, TMGa_1 and 2, DEZn_1


#morphology test at 650. Zn flow in barrier kept constant, so less Zn overall too. Buffer shortened.

#BHET 1st growth 1305 nm PL

#adjust T to 650 during "BUFFER". 10 minutes, no pause
# PL 1305
# SS
# XRD wide
# InP(S) wafers
# no timecode available



read Macro_Library; 

####################################################################
##      GENERAL INFORMATION
##      System 701884 - CRIUS-As/P (IC platform)
##      
##      No StateMachine
####################################################################


####################################################################
##      VARIABLES DEFINITION
####################################################################
##
##
##      TEMPERATURE CALIBRATION
##      Parameters used to calculate setpoints
##      for the requested surface temperatures

        variable Tcal_Factor = 1.136;
        variable Tcal_Offset = 89;
        variable ZoneA = 55;
        variable ZoneB = 59.7;
        variable ZoneC = 71.1;

        variable RampRate = 1.2;
        variable CoolRate = 0.9;

##      STATE-MACHINE CROSS-OVER POINTS
##      To optimise timing it is recommended
##      to match the StateMachine parameters

        variable Process_Start_Temp = 350;      # hydride start flowing
        variable Transfer_Auto_Temp = 300;      # hydride stop flowing at (Transfer_Auto_Temp + 50)

        variable Process_Start_ZoneA = 30;
        variable Process_Start_ZoneB = 50;
        variable Process_Start_ZoneC = 30;

        variable Process_Start_Press = 100;
        variable Transfer_Auto_Press = 50;
        variable Transfer_Auto_Flow = 12000;


##      GENERAL GROWTH CONDITIONS
        variable Growth_Temp = 746.2;
        variable Growth_Press = 132;
        variable Growth_Total_Flow = 48000;
	variable PH3_Stabilization = 800;
	
##	Oxide desorb
	variable Desorb_Temp = 812;
	variable Desorb_ZoneA = 51.2;
        variable Desorb_ZoneB = 58.6;
        variable Desorb_ZoneC = 67.6;
	variable Desorb_Press = 132;
	variable Desorb_PH3_flow = 800;
	variable Desorb_Time = 200;
	
	
	variable Cal_factor=0.960;

##	InP buffer
	variable Buffer_Temp = 770; ##650
	variable Buffer_ZoneA = 51.2;
        variable Buffer_ZoneB = 62;
        variable Buffer_ZoneC = 66.2;
	variable Buffer_Press = 132;
	variable Buffer_PH3_flow = 800;
	variable Buffer_Time = 600;
	variable Buffer_TMIn_1_flow = 696;
	variable Buffer_TMIn_2_flow = 696;
	variable Buffer_TMIn_total_flow = 1500;
	variable TMIn_1_conc=2899*Cal_factor;
	variable TMIn_2_conc=2873*0.975;
	variable Si_source_flow = 50;
	variable Si_dilute_flow = 300;
	variable Buffer_Si_inject_flow = 140; ## 20 2e18

	variable Q_PH3_flow = 800;
	variable Q_TMGa_source_flow = 100;
	variable Q_TMGa_dilute_flow = 900;
	variable Q_AsH3_source_flow = 90;
	variable Q_AsH3_dilute_flow = 200;

	variable DEZn_source = 100;
	variable DEZn_dilute= 300;


	variable TMGa_1_total_flow=200;
	variable TMGa_2_total_flow=1000;
	variable TMGa_2_conc=10450;



##	Q1050


	variable Q105_AsH3_inject_flow = 36.58; ## 0.2355 9.927
	variable Q105_TMGa_inject_flow = 50.648*Cal_factor; ## 0.1076 5.239
	variable Q105_Si_inject_flow = 48.65; ##6.95 8e17
	variable Q105_Time=30/0.5536;




##	Q1100
	variable Q11_AsH3_inject_flow=53.60; ##  0.3170 14.728
	variable Q11_TMGa_inject_flow = 71.239*Cal_factor; ## 0.1452 7.369
	variable Q11_Si_inject_flow = 30.31; ##4.33 5e17
	variable Q11_Time=30/0.578;


##	Q1120 at -0.2%

	variable Q112_AsH3_inject_flow = 64.41; ##  0.3559 17.78
	variable Q112_TMGa_source_flow = 9.392*Cal_factor; ## 0.1915 10.376tmga2 equivalent
	variable Q112_TMGa_1_conc=4823*Cal_factor;
	variable Q112_DEZn_inject_flow=26.96; ##6.74 5e17 
	variable Q112_Time=10/0.611; 

##	Q1333 at +1% to target 1305 PL


	variable Q14_AsH3_source_flow=35.37; ## 0.498
	variable Q14_TMGa_inject_flow = 40.41*Cal_factor; ## 0.0868 so 4.18
	variable Q14_Time=6/0.541;




## top InP

	variable InP_DEZn_inject_flow=36.72;	##5E17 so 9.18


##Q1.25
	variable Q125_AsH3_inject_flow=153.9814; ##  0.5386 so 43.05
	variable Q125_TMGa_inject_flow = 141.63*Cal_factor; ## 0.2484 so 14.65
	variable Q125_Time=4/0.657;












####################################################################


####################################################################
##      RECIPE CORE
####################################################################


layer {

   Basestate;           
                DORCheck;

          until TimerN2PurgeActive == off;


		Ini_1Macro;

        0.1 "set stuff",
		StepCode=1,
		DummyHyd_2.source follow AsH3_2.inject,
		DummyHyd_2.run follow [1-AsH3_2.run],
		DummyHyd_1.source = 500,
		DummyHyd_1.run follow [1-AsH3_1.run],
		DummyMO1_1.source follow [TMGa_1_total_flow],
		DummyMO1_1.run follow [1-TMGa_1.run],
		DummyMO1_2.source follow TMGa_2.inject,
		DummyMO1_2.run follow [1-TMGa_2.run],
		DummyDop_1.source follow Si2H6_1.inject,
		DummyDop_1.run follow [1-Si2H6_1.run],
		PH3_1.source = PH3_Stabilization,
		PH3_1.push = 1000-PH3_Stabilization;

        60      "Ramping to Process Flows",             
                Optical.purge to (Growth_Total_Flow  / 240),
                Liner.purge to (Growth_Total_Flow  / 6),
                Heater.purge to (Growth_Total_Flow  / 3),
                RunHyd1.feed1 follow (Growth_Total_Flow/2-[PH3_1.source+PH3_1.push]*[PH3_1.run]-DummyHyd_2.source-DummyHyd_1.source),
                RunMO1.feed1 follow (Growth_Total_Flow/6-DummyMO1_1.source-DummyMO1_2.source-1500*[TMIn_1.run]-1500*[TMIn_2.run]),
                RunMO1.feed2 to (Growth_Total_Flow  / 6),
                RunDop1.feed1 follow (Growth_Total_Flow / 6-DummyDop_1.source),
               RunHyd1.vent to  (Growth_Total_Flow  / 16),
                RunMO1.vent to  (Growth_Total_Flow  / 32),
                RunDop1.vent to  (Growth_Total_Flow  / 32);



	Ini_2Macro;


             
	DesorbMacro;

###	InP  buffer
	[(Desorb_Temp-Buffer_Temp)/CoolRate] "Cool down for InP buffer",
		Heater.temp to Buffer_Temp,
		HeaterzoneA.factor to Buffer_ZoneA-6,
                HeaterzoneB.factor to Buffer_ZoneB-4,
                HeaterzoneC.factor = Buffer_ZoneC+5,
		TMIn_1.line = on,
		TMIn_2.line = on;
		
	90	"Wait for stability",
		HeaterzoneA.factor to Buffer_ZoneA,
                HeaterzoneB.factor to Buffer_ZoneB,
		HeaterzoneC.factor to Buffer_ZoneC in 45,
		TMIn_1.source to Buffer_TMIn_1_flow in 45,
		TMIn_1.push to Buffer_TMIn_total_flow-Buffer_TMIn_1_flow in 45,
		TMIn_2.source to Buffer_TMIn_2_flow in 45,
		TMIn_2.push to Buffer_TMIn_total_flow-Buffer_TMIn_2_flow in 45,
		TMIn_1.totalflow = 1500,
		TMIn_1.setconc = TMIn_1_conc,
		TMIn_2.totalflow = 1500,
		TMIn_2.setconc = TMIn_2_conc,
		Si2H6_1.line=on,
		Si2H6_1.source=Si_source_flow,
		Si2H6_1.dilute=Si_dilute_flow,
		Si2H6_1.inject=Buffer_Si_inject_flow,	
		PH3_1.source to Buffer_PH3_flow,
		PH3_1.push to 100;
		
		
	20	"settle",
		TMIn_1.control = on,
		TMIn_2.control = on;
		
	[Buffer_Time-120] "BUFFER",
		TMIn_1.run = on,
		TMIn_2.run = on,
		Si2H6_1.run=on;
		
		

	60 "prepare Q layer, continue buffer",
		TMGa_2.line = open,
		TMGa_2.setconc = TMGa_2_conc,
		TMGa_2.totalflow = TMGa_2_total_flow,
		AsH3_2.source to Q_AsH3_source_flow,
		AsH3_2.dilute to Q_AsH3_dilute_flow,
		AsH3_2.inject to Q105_AsH3_inject_flow,
		AsH3_2.line = on,
		TMGa_2.source to Q_TMGa_source_flow,
		TMGa_2.dilute to Q_TMGa_dilute_flow,
		TMGa_2.inject to Q105_TMGa_inject_flow,
		TMGa_1.line=on,
		TMGa_1.control=on,
		TMGa_1.source = Q112_TMGa_source_flow,
		TMGa_1.push =[TMGa_1_total_flow-Q112_TMGa_source_flow],
		TMGa_1.totalflow = TMGa_1_total_flow,
		TMGa_1.setconc = Q112_TMGa_1_conc;
	




	60 "control on",
		TMGa_2.control=on,
		TMGa_1.control=on;
		



	[Q105_Time]	"Q1.05 growth",
		Si2H6_1.inject=Q105_Si_inject_flow,
		TMIn_1.run=on,
		TMGa_2.run=on,
		AsH3_2.run=on,
		Si2H6_1.run=on,
		TMIn_2.run=off,
		TMIn_2.line=off,
		TMIn_2.control=off;

	30 "reset flows",
		TMIn_1.run=off,
		TMGa_2.run=off,
		Si2H6_1.run=off,
		Si2H6_1.inject = Q11_Si_inject_flow,
		AsH3_2.inject = Q11_AsH3_inject_flow,
		TMGa_2.inject = Q11_TMGa_inject_flow;



	[Q11_Time]	"Q1.1 growth and set next Q",
		TMIn_1.run=on,
		TMGa_2.run=on,
		Si2H6_1.run=on,
		AsH3_1.line=on,
		AsH3_1.source=Q14_AsH3_source_flow,
		AsH3_1.push=500-Q14_AsH3_source_flow;

	30 "reset flows",
		TMGa_2.inject=Q14_TMGa_inject_flow,
		TMIn_1.run=off,
		TMGa_2.run=off,
		Si2H6_1.run=off,
		AsH3_2.inject = Q112_AsH3_inject_flow,
		DEZn_1.line=on,
		DEZn_1.source = DEZn_source,
		DEZn_1.dilute = DEZn_dilute,
		DEZn_1.inject = Q112_DEZn_inject_flow;




	[Q112_Time] "n doped Q112 growth and prepare QW",
		TMIn_1.run=on,
		TMGa_1.run=on,
		AsH3_2.run=on,
		Si2H6_1.run=on;


	

	[Q14_Time] "first well",
		TMGa_1.run=off,
		Si2H6_1.run=off,
		Si2H6_1.line=off,
		TMGa_2.run=on,
		AsH3_2.run=off,
		AsH3_1.run=on;






loop 5 	{



	[Q112_Time] "barrier",
		DEZn_1.run=on,
		TMGa_1.run=on,
		TMGa_2.run=off,
		AsH3_2.run=on,
		AsH3_1.run=off;





	[Q14_Time] "well",
		DEZn_1.run=off,
		TMGa_1.run=off,
		TMGa_2.run=on,
		AsH3_2.run=off,
		AsH3_1.run=on;
		

}








	[Q112_Time] " p doped barrier",
		TMGa_1.run=on,
		TMGa_2.run=off,
		AsH3_2.run=on,
		AsH3_1.run=off,
		AsH3_1.line=off,
		DEZn_1.run=on;



	20 "reset",
		TMIn_1.run=off,
		TMGa_1.run=off,
		TMGa_1.line=off,
		TMGa_1.control=off,
		DEZn_1.run=off,
		AsH3_2.inject = Q11_AsH3_inject_flow,
		TMGa_2.inject = Q11_TMGa_inject_flow,
		DEZn_1.inject=InP_DEZn_inject_flow;

	[Q11_Time/3] "Q11 layer",
		TMIn_1.run=on,
		TMGa_2.run=on,
		AsH3_2.run=on,
		DEZn_1.run=on;

		

	[60*1.03]	"first InP layer 30 nm",
		TMGa_2.run=off,
		AsH3_2.run=off,
		AsH3_2.inject=Q125_AsH3_inject_flow,
		TMGa_2.inject=Q125_TMGa_inject_flow;



	[Q125_Time] "Q1.25 etch stop",
		TMGa_2.run=on,
		AsH3_2.run=on;

	[30*1.03] "InP",
		TMGa_2.run=off,
		AsH3_2.run=off;

	[Q125_Time] "Q1.25 etch stop",
		TMGa_2.run=on,
		AsH3_2.run=on;

	[40*1.03] "InP cap",
		TMGa_2.run=off,
		TMGa_2.line=off,
		TMGa_2.control=off,
		AsH3_2.run=off,
		AsH3_2.line=off;



	[0.1]	"turn off",
		TMIn_1.run=off,
		TMIn_1.line=off,
		TMIn_1.control=off,
		DEZn_1.run=off,	
		DEZn_1.line=off;
		
			


    EndMacro;



    Basestate; 
    
    Pressure_Control_1200;
}
####################################################################

