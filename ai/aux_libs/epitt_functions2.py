import re
import os
import numpy as np
import pandas as pd
from glob import glob

def camel_to_snake(name):
    """CamelCase to snake_case

    Parameters
    ----------
    name : str
        Column name

    Returns
    -------
    str
        Renamed column in snake case format
    """
    name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', name).lower()


def epitt_parser(file_path):
    """Python parser for .dat docs (EPITT files)

    Parameters
    ----------
    file_path : str
        Path to the EPITT file

    Returns
    -------
    dict
        Python dictionary with variable and value pairs
    """
    with open(file_path, encoding="utf-8", errors="ignore") as f:
        text = f.read()

    string = text.split("BEGIN")[0]
    pat = r"##\w+\s?=\s?.+"
    sample = re.findall(pat, string)
    record = [x.replace("##", "") for x in sample if len(x) < 30]

    data = dict(
        zip(
            [x.split("=")[0].strip().lower() for x in record],
            [x.split("=")[-1].strip() for x in record],
        )
    )
    data["filename"] = os.path.basename(file_path)
    data = {x: y for x, y in data.items() if x not in ["xunits", "cols"]}

    string = text.split("BEGIN")[-1]
    record = string.split("\n")

    for i in range(len(record)):
        values = [x.strip() for x in record[i].split("\t")]
        if i == 0:
            values[0] = "BEGIN"
            cols = values
            nested_data = dict([(col, []) for col in cols])
        elif (i == len(record) - 1) | (i == len(record) - 2):
            pass
        else:
            for c, v in zip(cols, values):
                nested_data[c].append(float(v))

    nested_data = {camel_to_snake(x):y for x,y in nested_data.items()}
    data["nested_data"] = nested_data
    return data    


def epitt_cleaner(data):
    """Variables cleaner for .dat docs (EPITT files)

    Parameters
    ----------
    data : dict
        Python dictionary with variable and value pairs

    Returns
    -------
    dict
        Python dictionary with variable and value pairs cleaned
    """
    data["run"] = data["run_id"][:5]
    del data["run_id"]
    for var in data.keys():
        try:
            data[var] = float(data[var])
        except:
            pass
    data["wafer"] = int(data["wafer"])
    try:
        data["susceptor"] = int("".join([x for x in str(data["wafer_label"]) if x.isdigit()]))
        del data["wafer_label"]
    except:
        data["susceptor"] = None
    return data
