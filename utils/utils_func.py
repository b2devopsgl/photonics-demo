import pandas as pd
import subprocess
import PySimpleGUI as sg

def getJson(df):

    lista_ = []

    for i in range(0,len(df)):
        lista_temp = []
        lista_temp.append(df.iloc[i]['run'])
        lista_temp.append(df.iloc[i]['wafer'])
        lista_temp.append(df.iloc[i]['PL Prediction'])

        lista_.append(lista_temp)
        
    lista_resp = []

    for item in lista_:
        str1 = {'run' : str(item[0]), 'wafer' : str(item[1]), 'PL Prediction' : str(item[2])}
        lista_resp.append(str1)



    return lista_resp

def getFullPath():
    try:
        ClusterEstate = 'python utils/get_path.py'
        data = subprocess.check_output(ClusterEstate, shell=True, text=True)
        data_clean = data.replace("* Error performing wm_overrideredirect while hiding the hidden master root* expected boolean value but got \"\"\n","")
        data_clean = data_clean.replace("\n","")
        # print("data: "+data_clean)
        return data_clean
    except Exception as exc:
        # print('ERORR: ', exc)
        return 1