import os
from glob import glob
import pandas as pd
from datetime import datetime
import pytz
import numpy as np
import pickle
from pickle import dump
#import pickle5 as pickle
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import MinMaxScaler
from xgboost import XGBRegressor
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error

from ai.aux_libs.recipe_functions import recipe_cleaner,recipe_parser
from ai.aux_libs.epitt_functions2 import epitt_cleaner, epitt_parser
from ai.aux_libs.pl_functions import spm_cleaner, spm_parser


pd.options.mode.chained_assignment = None  #



#wireframe 5
def create_batches(batches_name, description, path_folder_test):
    """
    Parameters:
    batches_name: str
    Name the user has given to the introduced batch
    description:str
    Description the user has given to the introduced batch
    path_folder_test: str
    The path where the Recipe, EpiTT and SPM subfolder are living
    path_batches: str
    The path where the batches folders are living
    """
    dc_batches= {}
    path_batches = "ai/batches"
    #Folder is created inside the batches folder if it doesn't exist
    if not os.path.exists(os.path.join(path_batches,batches_name)):
        os.mkdir(os.path.join(path_batches,batches_name))
    ls_folder = os.listdir(path_folder_test)
    for folder in ls_folder:
        if "recipe" in folder.lower():
            path_recipe = folder
        if "epitt" in folder.lower():
            path_epitt = folder
        if "pl" in folder.lower():
            path_pl = folder
    ls_recipe = glob(os.path.join(path_folder_test, path_recipe, "*epi"))
    ls_epitt = glob(os.path.join(path_folder_test, path_epitt, "*dat"))
    ls_pl = glob(os.path.join(path_folder_test, path_pl, "*spm"))
    recipe_df = pd.DataFrame([recipe_cleaner(recipe_parser(file)) for file in ls_recipe])
    recipe_df["run"] = recipe_df["run"].str.upper()
    epitt_df = pd.DataFrame([epitt_cleaner(epitt_parser(file)) for file in ls_epitt])
    epitt_df['run'] = epitt_df["run"].str.upper()
    epitt_df.drop_duplicates(subset = ["wafer"], inplace = True)
    #spm = {"run":["C2922"]*6,"wafer":[1,2,3,4,5,6], "pl_median":[1320,1320,1320,1320,1320,1320]}
    spm = pd.DataFrame([spm_cleaner(spm_parser(file)) for file in ls_pl])
    #spm = pd.DataFrame(spm)
    spm = spm[["run","wafer","pl_median"]]
    spm["run"] = spm["run"].str.upper()
    spm["wafer"] = pd.to_numeric(spm["wafer"], errors="coerce")
    df_test = spm.merge(recipe_df, on="run", how="left").merge(epitt_df, on=["run", "wafer"], how="left")
    features = [
                 'ash3_1__source',
                 'tega_1__setconc',
                 'ash3_2__source',
                 'pyro_temp_ma_4',
                 'ash3_2__dilute',
                 'tmga_2__setconc',
                 'tmga_1__setconc',
                 'ash3_2__inject',
                 'tmga_2__inject',
                 'thickness' ]
    aux = pd.DataFrame([], columns = features)
    cols = df_test.columns[(df_test.columns.isin(aux.columns))]
    missing_cols = list(set(features).difference(set(cols)))
    data = df_test[cols]

    for missing in missing_cols:
        data.loc[:,missing] = np.nan
    data = data.join(df_test[["run","wafer", "pl_median"]])
    min_date = recipe_df["date"].min()
    max_date = recipe_df["date"].max()
    n_observations = len(data)
    dc_batches = {"Name":batches_name,
                  "Description":description,
                  "Min Date":min_date,
                  "Max Date": max_date,
                  "Observations Number":n_observations
                 }
    with open(f"ai/batches/{batches_name}/metadata.pkl","wb") as handle:
          pickle.dump(dc_batches, handle, protocol=pickle.HIGHEST_PROTOCOL)
    data.to_pickle(f"ai/batches/{batches_name}/batch.pkl")
    #print(data)
    return data


#wireframe 7
def get_batches_info():

    """
    Returns
    --------
    ls_dictionaries: list 
    List with the dictionaries from each batch 

    """
    ls_dictionaries = []
    ls_metadata = glob("ai/batches/*/metadata.pkl")
    for dc_path in ls_metadata:
        with open(dc_path, "rb") as handle:
            ls_dictionaries.append(pickle.load(handle))

    print(ls_dictionaries)
    return ls_dictionaries  


#wireframe 8
def getModelsInfo():

    """
    Returns
    --------
    ls_dictionaries: list 
    List with the dictionaries from each batch 

    """
    ls_dictionaries = []
    ls_metadata = glob("ai/model_results/*/metadata_model.pkl")
    for dc_path in ls_metadata:
        with open(dc_path, "rb") as handle:
            ls_dictionaries.append(pickle.load(handle))

    print(ls_dictionaries)
    return ls_dictionaries      


# wireframe 7 boton retrain
def create_train_data_from_batch(batches_name, min_obs_split = 50):
    """
    Parameters
    batches_name: list 
    The list with the name of the batches 

    min_obs_split:int 
    The minimum observations the batch needs to have for splitting into train and test 
    """
    batches_name.insert(0,'_batch_base_')
    
    X_train, X_test, y_train, y_test = tuple([pd.DataFrame()]*4)
    for batch in batches_name:
        
        if batch == "batch_base":
            continue
        
        X_y = pd.read_pickle(f"ai/batches/{batch}/batch.pkl")
        X = X_y.drop(columns = ["pl_median"])
        y = X_y[["pl_median"]]
        del X_y 
        if batch == "_batch_base_" and X.shape[0] > min_obs_split:
            #TODO para la fase productivo este if es innecesario en la parte de batch_base 
            
            X_train_temp, X_test_temp, y_train_temp, y_test_temp = train_test_split(X,y, random_state = 8921739)
            X_train, X_test, y_train, y_test = X_train.append(X_train_temp), X_test.append(X_test_temp), y_train.append(y_train_temp), y_test.append(y_test_temp)
        else:    
            X_train_temp, y_train_temp= X,y
            X_train, y_train = X_train.append(X_train_temp), y_train.append(y_train_temp)
    if y_test.shape[0] == 0:
        X_y = pd.read_pickle(f"ai/batches/_batch_base_/batch.pkl")
        X = X_y.drop(columns = ["pl_median"])
        y = X_y[["pl_median"]]
        del X_y 
        _, X_test, _ , y_test = train_test_split(X,y, random_state = 8921739)
        
    #print(X_train)
    #print(X_test)
    #print(y_train)
    #print(y_test)
    return X_train, X_test, y_train, y_test, batches_name     
            

# def retrain_algorithm(model_id, X_train, X_test, y_train, y_test, used_batches):
#     """
#     Parameters
#     ----------
#     model_name: str
#     This will be the identifier of the model
    
#     X_train: pd.DataFrame
#     The data which will train the model, it must come over Run and Wafer and the features 
#     are gonna be from the recipe
    
#     X_test: pd.DataFrame
#     The test data that will make the predictions over the target variable 
    
#     y_train: pd.DataFrame
#     The target variable used for training the model 
    
    
#     y_test:pd.DataFrame
#     The target variable used for testing the model 
    
#     Used_batches: list
#     The name of the used batches to retrain within a list 
    
    
#     Returns
#     -------
    
#     model_file: joblib 
#     File with the model saved 
    
#     model_metada: Dictionary 
#     The metadata from the model 
#     Keys: {model_id, Used batches , date, rmse, mae}
    
#     """
    
    
    
#     model_metadata = {}

#     #Used Variables 
    
#     features = [
#                  'ash3_1__source',
#                  'tega_1__setconc',
#                  'ash3_2__source',
#                  'pyro_temp_ma_4',
#                  'ash3_2__dilute',
#                  'tmga_2__setconc',
#                  'tmga_1__setconc',
#                  'ash3_2__inject',
#                  'tmga_2__inject',
#                  'thickness' ]
    
#     #Date 

#     date = datetime.now()
#     day_string = datetime.now().date().strftime("%Y_%m_%d")
    
#     #Used Batches 

#     #A minimum of 8 nulls is required
    
#     #train = train[((train[features].isna()).sum(axis =1) < )]
#     X_train = X_train[features]
#     X_test= X_test[features]
    

    
#     #Imputing data 

#     imputer = SimpleImputer(strategy= "median")
#     imputer.fit(X_train)
#     Xi_train = pd.DataFrame(imputer.transform(X_train), columns = features)
#     Xi_test = pd.DataFrame(imputer.transform(X_test), columns = features)

#     #Scaling 

#     sc = MinMaxScaler() #Scaler for X
#     scy = MinMaxScaler() #Scaler for y 
#     #Training
#     sc.fit(Xi_train) 
#     scy.fit(y_train)
#     #Transforming train and test 
#     Xs_train = pd.DataFrame(sc.transform(Xi_train),columns=features)
#     ys_train = pd.DataFrame(scy.transform(y_train[["pl_median"]]),columns=["pl_median"])
#     Xs_test = pd.DataFrame(sc.transform(Xi_test),columns=features)
#     ys_test = pd.DataFrame(scy.transform(y_test[["pl_median"]]),columns=["pl_median"])

#     #Modeling 
    

#     model = XGBRegressor(booster = "dart")
#     model.fit(Xs_train,ys_train)


#     #Metrics 
#     y_true_test = y_test.values.ravel()
#     y_pred_test = scy.inverse_transform(model.predict(Xs_test).reshape(-1,1)).ravel()
#     rmse = mean_squared_error(y_true=y_true_test, y_pred=y_pred_test, squared=False)
#     mae = mean_absolute_error(y_true=y_true_test, y_pred=y_pred_test)

#     model_file = dump(model,f'model_{day_string}.joblib')
#    # model_file = dump(model, "model_1.joblib")

    
#     model_metadata["model_id"] = model_id 
#     model_metadata["rmse"] = rmse
#     model_metadata["mae"] = mae 
#     model_metadata["Used Batches"] = ", ".join(used_batches)
#     model_metadata["Execution Date"] = date 


#     return model_file, model_metadata



# def retrain_algorithm(model_id, X_train, X_test, y_train, y_test, used_batches):
#     """
#     Parameters
#     ----------
#     model_id: str
#     This will be the identifier of the model
    
#     X_train: pd.DataFrame
#     The data which will train the model, it must come over Run and Wafer and the features 
#     are gonna be from the recipe
    
#     X_test: pd.DataFrame
#     The test data that will make the predictions over the target variable 
    
#     y_train: pd.DataFrame
#     The target variable used for training the model 
    
    
#     y_test:pd.DataFrame
#     The target variable used for testing the model 
    
#     Used_batches: list
#     The name of the used batches to retrain within a list 
    
    
#     Returns
#     -------
    
#     model_file: joblib 
#     File with the model saved 
    
#     model_metada: Dictionary 
#     The metadata from the model 
#     Keys: {model_id, Used batches , date, rmse, mae}
    
#     """
    
    
    
#     model_metadata = {}
#     path_result = "ai/model_results/"

#     if not os.path.exists(os.path.join(path_result,model_id)):
#         os.mkdir(os.path.join(path_result,model_id))


#     #Used Variables 
    
#     features = [
#                  'ash3_1__source',
#                  'tega_1__setconc',
#                  'ash3_2__source',
#                  'pyro_temp_ma_4',
#                  'ash3_2__dilute',
#                  'tmga_2__setconc',
#                  'tmga_1__setconc',
#                  'ash3_2__inject',
#                  'tmga_2__inject',
#                  'thickness' ]
    
#     #Date 
#     date = datetime.now(pytz.timezone('America/Monterrey'))
#     day_string = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
#     #Used Batches 
#     #A minimum of 8 nulls is required
    
#     #train = train[((train[features].isna()).sum(axis =1) < )]
#     X_train = X_train[features]
#     X_test= X_test[features]
    
    
#     #Imputing data 
#     imputer = SimpleImputer(strategy= "median")
#     imputer.fit(X_train)
    
#     Xi_train = pd.DataFrame(imputer.transform(X_train), columns = features)
#     Xi_test = pd.DataFrame(imputer.transform(X_test), columns = features)
#     #Scaling 
#     sc = MinMaxScaler() #Scaler for X
#     scy = MinMaxScaler() #Scaler for y 
#     #Training
#     sc.fit(Xi_train) 
#     scy.fit(y_train)
#     #Transforming train and test 
#     Xs_train = pd.DataFrame(sc.transform(Xi_train),columns=features)
#     ys_train = pd.DataFrame(scy.transform(y_train[["pl_median"]]),columns=["pl_median"])
#     Xs_test = pd.DataFrame(sc.transform(Xi_test),columns=features)
#     ys_test = pd.DataFrame(scy.transform(y_test[["pl_median"]]),columns=["pl_median"])
#     #Modeling 
    
#     model = XGBRegressor(booster = "dart")
#     model.fit(Xs_train,ys_train)
#     #Metrics 
#     y_true_test = y_test.values.ravel()
#     y_pred_test = scy.inverse_transform(model.predict(Xs_test).reshape(-1,1)).ravel()
#     rmse = mean_squared_error(y_true=y_true_test, y_pred=y_pred_test, squared=False)
#     mae = mean_absolute_error(y_true=y_true_test, y_pred=y_pred_test)

#     #dump(model,(f'model_results/model_{model_id}.joblib','wb'))

#     with open(f'ai/model_results/{model_id}/model_{model_id}.joblib', 'wb') as pickle_file_model:
#         dump(model, pickle_file_model)

#     #dump(imputer,(f'model_results/imputer_{model_id}.joblib', 'wb'))

#     with open(f'ai/model_results/{model_id}/imputer_{model_id}.joblib', 'wb') as pickle_file_imputer:
#         dump(imputer, pickle_file_imputer)


#     #dump(sc,f'model_results/scalerx_{model_id}.joblib')

#     with open(f'ai/model_results/{model_id}/scalerx_{model_id}.joblib', 'wb') as pickle_file_sc:
#         dump(sc, pickle_file_sc)

#     #dump(scy,f'model_results/scalery_{model_id}.joblib')

#     with open(f'ai/model_results/{model_id}/scalery_{model_id}.joblib', 'wb') as pickle_file_scy:
#         dump(scy, pickle_file_scy)
        
#     model_metadata["model_id"] = model_id 
#     model_metadata["rmse"] = rmse
#     model_metadata["mae"] = mae 
#     model_metadata["Used Batches"] = len(used_batches)
#     model_metadata["Execution Date"] = day_string 
    
   
#     with open(f'ai/model_results/{model_id}/metadata_{model_id}.pkl','wb') as handle_meta:
#         dump(model_metadata, handle_meta)

#     print(model_metadata)
#     return model_metadata


def retrain_algorithm_new(model_id, X_train, X_test, y_train, y_test, used_batches):
    """
    Parameters
    ----------
    model_name: str
    This will be the identifier of the model
   
    X_train: pd.DataFrame
    The data which will train the model, it must come over Run and Wafer and the features
    are gonna be from the recipe
   
    X_test: pd.DataFrame
    The test data that will make the predictions over the target variable
   
    y_train: pd.DataFrame
    The target variable used for training the model
   
   
    y_test:pd.DataFrame
    The target variable used for testing the model
   
    Used_batches: list
    The name of the used batches to retrain within a list
   
   
    Returns
    -------
   
    model_file: joblib
    File with the model saved
   
    model_metada: Dictionary
    The metadata from the model
    Keys: {model_id, Used batches , date, rmse, mae}
   
    """
   
   
   
    model = pd.read_pickle("ai/aux_libs/joblib/model.pkl")
    scaler_target = pd.read_pickle("ai/aux_libs/joblib/scaler_target.pkl")
    pipeline_pre = pd.read_pickle("ai/aux_libs/joblib/pipeline_pre.pkl")
    features = pd.read_pickle("ai/aux_libs/joblib/features.pkl")
   
    model_metadata = {}
    path_result = "ai/model_results"
   
    if not os.path.exists(os.path.join(path_result,model_id)):
        os.mkdir(os.path.join(path_result,model_id))

    #Used Variables
   
   
   
    #Date

    date = datetime.now(pytz.timezone('America/Monterrey'))
    day_string = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
   
    #Used Batches

    #A minimum of 8 nulls is required
   
    X_train = X_train[features]
    X_test= X_test[features]



    X_train.loc[X_train["ash3_1__source"].isna() & (~X_train["ash3_2__source"].isna()), "ash3_1__source"] = 0
    X_train.loc[(~X_train["ash3_1__source"].isna()) & (X_train["ash3_2__source"].isna()), "ash3_2__source"] = 0
    X_train.loc[X_train["tmga_1__setconc"].isna() & (~X_train["tmga_2__setconc"].isna()), "tmga_1__setconc"] = 0
    X_train.loc[(~X_train["tmga_1__setconc"].isna()) & (X_train["tmga_2__setconc"].isna()), "tmga_2__setconc"] = 0

    X_test.loc[X_test ["ash3_1__source"].isna() & (~X_test ["ash3_2__source"].isna()), "ash3_1__source"] = 0
    X_test.loc[(~X_test ["ash3_1__source"].isna()) & (X_test ["ash3_2__source"].isna()), "ash3_2__source"] = 0
    X_test.loc[X_test ["tmga_1__setconc"].isna() & (~X_test ["tmga_2__setconc"].isna()), "tmga_1__setconc"] = 0
    X_test.loc[(~X_test ["tmga_1__setconc"].isna()) & (X_test ["tmga_2__setconc"].isna()), "tmga_2__setconc"] = 0

    Xs_train = pd.DataFrame(data=pipeline_pre.transform(X_train), columns=features)
    ys_train = pd.DataFrame(scaler_target.transform(y_train[["pl_median"]]),columns=["pl_median"])
    Xs_test =  pd.DataFrame(data=pipeline_pre.transform(X_test), columns=features)
    ys_test = pd.DataFrame(scaler_target.transform(y_test[["pl_median"]]),columns=["pl_median"])


    #Modeling

    model.fit(Xs_train,ys_train)

    #Metrics
    y_true_test = y_test.values.ravel()
    #y_pred_test = scy.inverse_transform(model.predict(Xs_test).reshape(-1,1)).ravel()
    y_pred_test = scaler_target.inverse_transform(model.predict(Xs_test).reshape(-1,1)).ravel()
    rmse = mean_squared_error(y_true=y_true_test, y_pred=y_pred_test, squared=False)
    mae = mean_absolute_error(y_true=y_true_test, y_pred=y_pred_test)


    true_test = pd.DataFrame(zip(y_true_test,y_pred_test)).rename(columns = {0:"pl_true",1:"pl_pred"})

   
    model_metadata["model_id"] = model_id
    model_metadata["rmse"] = rmse
    model_metadata["mae"] = mae
    model_metadata["Used Batches"] = len(used_batches)
    model_metadata["Execution Date"] = day_string
   
   
    with open(f"ai/model_results/{model_id}/metadata_model.pkl","wb") as handle:
         pickle.dump(model_metadata, handle, protocol=4)
   
    with open(f"ai/model_results/{model_id}/model.pkl","wb") as handle:
         pickle.dump(model, handle, protocol=4)        


    true_test.to_pickle(f"ai/model_results/{model_id}/true_test.pkl")
   
    return model_metadata





#wireframe 8
#def get_models_info():
# Mostrar el MAE

        
#create_batches("Batch_Test", "Just test batch", "/Users/ecastillo/Desktop/folder_test_retrain/")
#get_batches_info()
#X_train, X_test, y_train, y_test, batches_name  = create_train_data_from_batch(["Batch_Test","_batch_base_"])
#retrain_algorithm("modelTEST", X_train, X_test, y_train, y_test, batches_name)